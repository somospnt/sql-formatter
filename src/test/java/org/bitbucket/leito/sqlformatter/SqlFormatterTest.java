/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import org.apache.commons.io.IOUtils;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class SqlFormatterTest {

    private SqlFormatter formatter;

    private String readFromResource(String name) throws Exception {
        return IOUtils.toString(App.class.getResourceAsStream(name), "utf-8");
    }

    @Before
    public void setup() {
        formatter = new SqlFormatter();
    }

    private void assertInsertStatementHeader(String tableName, String sql) {
        assertTrue(sql.contains("\n-- " + tableName + "\n"));
    }

    @Test
    public void format_complexInsertFile_formatsSql() throws Exception {
        String formattedSql = formatter.format(readFromResource("/insert.sql"));
        System.out.println(formattedSql);
        assertTrue(formattedSql.length() > 100);

        assertInsertStatementHeader("movie", formattedSql);
        assertTrue(formattedSql.contains("INSERT INTO movie\n(id  , title                       , release_date, enabled, id_genre) VALUES\n"));
        assertTrue(formattedSql.contains("(2   , 'The Place Beyond The Pines', '2011-11-04', false  , NULL    ),\n"));
        assertTrue(formattedSql.contains("(3   , 'Grand Hotel Budapest'      , '2011-11-04', false  , 5       ),\n"));
        assertTrue(formattedSql.contains("(6   , 'Inglorious Basterds'       , NULL        , true   , NULL    ),\n"));
        assertTrue(formattedSql.contains("(7   , 'áéíóú // ÁÉÍÓÚ // ñÑ'      , '2016-11-04', false  , NULL    ),\n"));
        assertTrue(formattedSql.contains("(999 , 'The Last Movie'            , NULL        , NULL   , NULL    );\n"));

        assertInsertStatementHeader("genre", formattedSql);
        assertTrue(formattedSql.contains("INSERT INTO genre\n(id  , description , enabled) VALUES\n"));
        assertTrue(formattedSql.contains("(2   , 'Terror'    , NULL   ),\n"));
        assertTrue(formattedSql.contains("(7   , 'Documental', false  ),\n"));
        assertTrue(formattedSql.contains("(99  , 'Last Genre', NULL   );\n"));
    }

    @Test
    public void format_fileWithCreateStatements_formatsSql() throws Exception {
        String formattedSql = formatter.format(readFromResource("/create.sql"));
        assertTrue(formattedSql.contains("CREATE TABLE movie (\n"));
        assertTrue(formattedSql.contains("    id bigint,\n"));
        assertTrue(formattedSql.contains("    title varchar (255) not null,\n"));
        assertTrue(formattedSql.contains("    release_date datetime,\n"));
        assertTrue(formattedSql.contains("    enabled boolean,\n"));
        assertTrue(formattedSql.contains("    genre_id bigint references genre (id),\n"));
        assertTrue(formattedSql.contains("    UNIQUE (title, release_date, enabled),\n"));
        assertTrue(formattedSql.contains("    CONSTRAINT pk_movie PRIMARY KEY (id)\n"));
        assertTrue(formattedSql.contains(");\n"));

        assertTrue(formattedSql.contains("CREATE TABLE genre (\n"));
        assertTrue(formattedSql.contains("    id bigint primary key,\n"));
        assertTrue(formattedSql.contains("    description varchar (255) not null,\n"));
        assertTrue(formattedSql.contains("    enabled boolean\n"));
        assertTrue(formattedSql.contains(");\n"));
    }

    @Test
    public void format_fileWithMixedStatements_formatsStatementsOtherThanInsertAndCreateInSingleLineAndLeavesNonSupported() throws Exception {
        String formattedSql = formatter.format(readFromResource("/mixed.sql"));
        String expectedSql = readFromResource("/mixed_formatted.sql");
        assertTrue(formattedSql.contains(expectedSql));
    }

}
