CREATE TABLE movie (id bigint,title varchar (255) not null,
release_date datetime,enabled boolean,
genre_id bigint references genre (id),
UNIQUE (title, release_date, enabled),CONSTRAINT pk_movie PRIMARY KEY (id));
CREATE TABLE genre (id bigint primary key,description varchar (255) not null,enabled boolean);

