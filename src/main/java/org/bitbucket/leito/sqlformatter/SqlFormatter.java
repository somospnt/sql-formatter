/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.expression.operators.relational.MultiExpressionList;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.insert.Insert;

/**
 * A class to formats and groups SQL content.
 */
public class SqlFormatter {

    /**
     * The list of insert statements to format. The key is the name of the
     * table.
     */
    private Map<String, InsertStatementFormatter> insertStatements = new LinkedHashMap<>();
    private StringBuilder otherStatements = new StringBuilder();

    /**
     * Formats the sql script. This method formats SQL statements. It groups all
     * INSERT for the same table into one statement and then it formats all the
     * columns so that its easier to read. It indents CREATE TABLE and
     * leaves any other statement as is.
     *
     * @param sql the SQL script to format.
     * @return a formatted sql script.
     */
    public String format(String sql) {
        String[] stringStatements = sql.split("(?<=;)");
        for (String stringStatement : stringStatements) {
            try {
                Statement statement = CCJSqlParserUtil.parse(stringStatement);
                if (statement instanceof Insert) {
                    Insert insert = (Insert) statement;
                    String tableName = insert.getTable().getName();
                    addInsertStatement(tableName);
                    addRowValues(tableName, insert.getColumns(), insert.getItemsList());
                } else if (statement instanceof CreateTable) {
                    addCreateStatement((CreateTable) statement);
                } else {
                    addMiscellaneousStatement(statement);
                }
            } catch (Exception ex) {
                addUnsupportedStatement(stringStatement);
            }
        }
        return formatStatements();
    }

    private void addInsertStatement(String tableName) {
        insertStatements
                .putIfAbsent(tableName, new InsertStatementFormatter(tableName));
    }

    private void addCreateStatement(CreateTable statement) {
        otherStatements
                .append(CreateTableStatementFormatter.format(statement))
                .append("\n");
    }

    private void addMiscellaneousStatement(Statement statement) {
        otherStatements
                .append(DefaultStatementFormatter.format(statement));
    }

    private void addUnsupportedStatement(String stringStatement) {
        String trimmedStatement = stringStatement.trim();
        if (!trimmedStatement.isEmpty()) {
            otherStatements
                    .append(trimmedStatement)
                    .append("\n\n");
        }
    }

    private void addRowValues(String tableName, List<Column> columns, ItemsList itemsList) {
        if (itemsList instanceof ExpressionList) {
            ExpressionList values = (ExpressionList) itemsList;
            addRowValues(tableName, columns, values);
        } else if (itemsList instanceof MultiExpressionList) {
            MultiExpressionList mel = (MultiExpressionList) itemsList;
            mel.getExprList().forEach(values -> addRowValues(tableName, columns, values));
        } else {
            throw new UnsupportedOperationException("Type not supported: " + itemsList.getClass());
        }
    }

    private void addRowValues(String tableName, List<Column> columns, ExpressionList values) {
        InsertStatementFormatter table = insertStatements.get(tableName);
        table.addRowValues(columns, values);
    }

    private String formatStatements() {
        StringBuilder sb = new StringBuilder();

        sb.append(otherStatements);
        for (InsertStatementFormatter insertStatement : insertStatements.values()) {
            sb.append(insertStatement.format()).append("\n");
        }
        return sb.toString();
    }
}
